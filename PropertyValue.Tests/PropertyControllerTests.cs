using System;
using Xunit;
using PropertyValue.Services;
using PropertyValue.Models;
using PropertyValue.Controllers;
using Moq;
using Microsoft.AspNetCore.Mvc;
using Shared.Models;
using System.Threading.Tasks;

namespace PropertyValue.Tests
{
  public class PropertyControllerTests
  {
    [Fact]
    public async Task PropertyController_PostOk()
    {
      var responseTask = new SquareMeterDtoResponse
      {
        Successful = true,
        Data = new Services.SquareMeterDto
        {
          Value = 10
        }
      };
      var mockServiceApi = new Mock<ISquareMeterPriceClientApi>();
      mockServiceApi.Setup(m => m.SquareMeterAsync(default(System.Threading.CancellationToken))).Returns(Task.FromResult(responseTask));
      var mockService = new PropertyService();
      var controller = new PropertyController(mockServiceApi.Object, mockService);
      var input = new PropertyInput
      {
        SquareMeters = 10
      };

      var result = await controller.Post(input);

      Assert.IsType<ActionResult<Response<PropertyDto>>>(result);
    }

    [Fact]
    public async Task PropertyController_PostError()
    {
      var responseTask = new SquareMeterDtoResponse
      {
        Successful = false,
        Data = null
      };
      var mockServiceApi = new Mock<ISquareMeterPriceClientApi>();
      mockServiceApi.Setup(m => m.SquareMeterAsync(default(System.Threading.CancellationToken))).Returns(Task.FromResult(responseTask));
      var mockService = new PropertyService();
      var controller = new PropertyController(mockServiceApi.Object, mockService);
      var input = new PropertyInput
      {
        SquareMeters = 10
      };

      var result = await controller.Post(input);

      Assert.IsType<ActionResult<Response<PropertyDto>>>(result);
    }
  }
}
