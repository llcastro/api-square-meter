using System;
using Xunit;
using PropertyValue.Services;
using PropertyValue.Models;

namespace PropertyValue.Tests
{
  public class PropertyServiceTests
  {
    [Fact]
    public void PropertyService_GetPropertyValue()
    {
      var service = new PropertyService();
      var input = new PropertyInput
      {
        SquareMeters = 10
      };
      var result = service.GetPropertyValue(input, 10);

      Assert.True(result.SquareMeters == 10, "GetPropertyValue.SquareMeters should be 10");

      Assert.True(result.PropertyValue == 100, "GetPropertyValue.PropertyValue should be 100");
    }
  }
}
