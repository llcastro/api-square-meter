using PropertyValue.Models;

namespace PropertyValue.Services
{
  public class PropertyService : IPropertyService
  {
    public PropertyDto GetPropertyValue(PropertyInput input, double SquareMeterValue)
    {
      var property = new PropertyDto
      {
        SquareMeters = input.SquareMeters,
        PropertyValue = SquareMeterValue * input.SquareMeters
      };

      return property;
    }
  }
}
