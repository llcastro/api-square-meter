using System;
using PropertyValue.Models;

namespace PropertyValue.Services
{
  public interface IPropertyService
  {
    PropertyDto GetPropertyValue(PropertyInput input, double SquareMeterValue);
  }
}
