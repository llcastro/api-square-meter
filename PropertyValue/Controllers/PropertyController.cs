﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PropertyValue.Services;
using PropertyValue.Models;
using Shared.Models;

namespace PropertyValue.Controllers
{
  [ApiController]
  [Route("[controller]")]
  public class PropertyController : ControllerBase
  {
    private ISquareMeterPriceClientApi _clientApi;
    private IPropertyService _service;

    public PropertyController(ISquareMeterPriceClientApi clientApi, IPropertyService service)
    {
      _clientApi = clientApi;
      _service = service;
    }

    [HttpPost]
    public async Task<ActionResult<Response<PropertyDto>>> Post([FromBody] PropertyInput input)
    {
      var square = await _clientApi.SquareMeterAsync();
      if (!square.Successful)
      {
        return BadRequest(new Response<string> { Errors = square.Errors, Successful = false });
      }
      var property = _service.GetPropertyValue(input, square.Data.Value);

      var response = new Response<PropertyDto>()
      {
        Successful = true,
        Data = property
      };

      return Ok(response);
    }
  }
}
