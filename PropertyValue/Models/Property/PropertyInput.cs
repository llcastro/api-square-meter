using FluentValidation;

namespace PropertyValue.Models
{
  public class PropertyInput
  {
    public double SquareMeters { get; set; }
  }

  public class PropertyInputValidator : AbstractValidator<PropertyInput>
  {
    public PropertyInputValidator()
    {
      RuleFor(x => x.SquareMeters).InclusiveBetween(10, 10000);
    }
  }
}