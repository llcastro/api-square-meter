namespace PropertyValue.Models
{
  public class PropertyDto
  {
    public double SquareMeters { get; set; }
    public double PropertyValue { get; set; }
  }
}