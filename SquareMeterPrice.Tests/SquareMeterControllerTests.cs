using Xunit;
using SquareMeterPrice.Services;
using Moq;
using SquareMeterPrice.Controllers;
using Microsoft.AspNetCore.Mvc;
using Shared.Models;

namespace SquareMeterPrice.Tests
{
  public class SquareMeterControllerTests
  {
    [Fact]
    public void SquareMeterController_Get()
    {
      var mockService = new Mock<ISquareMeterService>();
      var controller = new SquareMeterController(mockService.Object);

      var result = controller.Get();

      Assert.IsType<ActionResult<Response<SquareMeterDto>>>(result);
    }
  }
}
