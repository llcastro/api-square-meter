using Xunit;
using SquareMeterPrice.Services;

namespace SquareMeterPrice.Tests
{
  public class SquareMeterServiceTests
  {
    [Fact]
    public void SquareMeterService_GetSquareMeter()
    {
      var service = new SquareMeterService();
      for (var x = 0; x < 10; x++)
      {
        var result = service.GetSquareMeter();

        Assert.True(result.Value >= 20 || result.Value <= 100, "GetSquareMeter should be between 20 and 100");
      }
    }
  }
}
