using System.Collections.Generic;

namespace Shared.Models
{
  public class Response<T>
  {
    public bool Successful { get; set; }
    public IEnumerable<string> Errors { get; set; }
    public T Data { get; set; }
  }
}