using System;
using Shared.Models;

namespace SquareMeterPrice.Services
{
  public interface ISquareMeterService
  {
    SquareMeterDto GetSquareMeter();
  }
}
