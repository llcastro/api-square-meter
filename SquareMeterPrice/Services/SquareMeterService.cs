using System;
using Shared.Models;

namespace SquareMeterPrice.Services
{
  public class SquareMeterService : ISquareMeterService
  {
    public SquareMeterDto GetSquareMeter()
    {
      var rng = new Random();
      var square = new SquareMeterDto
      {
        Value = rng.Next(20, 100)
      };

      return square;
    }
  }
}
