﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Shared.Models;
using SquareMeterPrice.Services;

namespace SquareMeterPrice.Controllers
{
  [ApiController]
  [Route("[controller]")]
  public class SquareMeterController : ControllerBase
  {
    private ISquareMeterService _service;

    public SquareMeterController(ISquareMeterService service)
    {
      _service = service;
    }

    [HttpGet]
    public ActionResult<Response<SquareMeterDto>> Get()
    {
      var square = _service.GetSquareMeter();

      var response = new Response<SquareMeterDto>()
      {
        Successful = true,
        Data = square
      };

      return Ok(response);
    }
  }
}
