# Estrutura

- O projeto **SquareMeterPrice** é referente a API de valor do métro quadrado.
- O projeto **PropertyValue** é referente a API do cálculo do valor do imóvel.
- O Projeto **Shared** possui arquivos compartilhados entre os projetos.
- O build e deploy dos dois projetos é automatico pelo **GitLab CI**.
- Os projetos estão com deploy no **Heroku** em containers **Docker**.
- Os projetos são WebApi's utilizando **.NET Core**.

# Projeto SquareMeterPrice

- Está disponível em [SquareMeterPrice](https://squaremeterprice.herokuapp.com/swagger/index.html)
- Possui apenas um controller com uma função que retorna o valor do métro quadrado, este valor é gerado aleatóriamente entre 20 e 100.

# Projeto PropertyValue

- Está disponível em [PropertyValue](https://property-value.herokuapp.com/swagger/index.html)
- Possui apenas um controller com uma função que recebe a quantidade de métros quadrados do imóvel e retorna o valor do imóvel.
- Para calcular o valor do imóvel, é utilizado a **API** do projeto **PropertyValue** para buscar o valor do métro quadrado.
- Para contruir a API do cliente do projeto **PropertyValue** foi usado o [NSwagStudio](https://github.com/RicoSuter/NSwag/wiki/NSwagStudio) e o arquivo gerado automaticamente através do **JSON** do **Swagger** encontra-se dentro da pasta **Services**.
- A rota recebe um parâmetro **PropertyInput**, dentro desta classe tem a classe de validação chamada **PropertyInputValidator** que utiliza o [FluentValidation](https://docs.fluentvalidation.net/en/latest/aspnet.html) para validar.
- Dentro do **appsettings.json** do projeto tem a definição da **API** que retorna o valor do métro quadrado, para testar localmente é necessário alterar este valor.

# Build e Deploy

- Cada projeto tem o seu próprio **Dockerfile**.
- O script de build e deploy está no arquivo **gitlab-ci.yml**.
